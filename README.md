# Webpage Haunted Castles

Multiple pages made for course Open Computing. The goal was only to learn the used
technologies so most of the code could be written better. 

The pages include searching through XML file depending on the filters that the user has set.
Also some data is fetched from three different APIs using php. The used APIs are:
*  Wikipedia REST API
*  Wikipedia Action API
*  Nominatim


