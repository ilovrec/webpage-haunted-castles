<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:template match="/podaci">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <title>Ukleti dvorci Europe</title>
                <meta charset="UTF-8" />
                <link rel="stylesheet" type="text/css" href="./dizajn.css" />
				<link rel="stylesheet" type="text/css" href="./dizajn_xml.css" />
            </head>
            <body>
                <header>
      <figure>
        <a href="./index.html" class="naslovna">
          
        </a>
      </figure>
      <h1>Ukleti dvorci Europe</h1>
    </header>
	
	<div class="glavni_dio">

	<nav class="navigacija">
	<ul id="nav_lista">
	<li> <a href="index.html" class="nav_button">Početna stranica</a> </li>
	<li> <a href="obrazac.html" class="nav_button">Pretraživanje</a> </li>
	<li> <a href="podaci.xml" class="nav_button">Podaci o dvorcima</a> </li>
	<li> <a href="http://www.fer.unizg.hr/predmet/or" class="nav_button">Predmet Otvoreno računarstvo</a> </li>
	<li> <a href="http://www.fer.unizg.hr" target="_blank" class="nav_button">FER početna stranica</a> </li>
	<li> <a href="mailto:iva.lovrec@fer.hr" class="nav_button"> E-mail autora</a> </li>
	
	</ul>
	</nav> 
	
    <article id="data_glavno">
	<h2> Podaci o dvorcima </h2>
	<table border="3" align="center" margin="100px"> 
			<tr> 
			<th>Naziv</th> 
			<th>Godina izgradnje</th> 
			<th>Mjesto</th> 
			<th>Država</th> 
			<th>Vlasništvo</th>
			<th>Mogućnost posjete</th>
			<th>Službena stranica</th>
			</tr> 
		
		<xsl:for-each select="/podaci/dvorac"> 
		<tr onmouseover="promijeniBoju(this)"> 
		<script type="text/javascript">
		function promjeniBoju(row){
	row.style.backgroundColor ="#FFFFFF";
	row.style.visibility="visible";
	row.style.color="green";
	</script>
		<td><xsl:value-of select="naziv"/></td> 
		<td><xsl:value-of select="izgradnja"/></td> 
		<td><xsl:value-of select="adresa/mjesto"/></td> 
		<td><xsl:value-of select="adresa/drzava"/></td>
		<td><xsl:value-of select="@vlasnik"/></td>
		<td><xsl:value-of select="@otvorenost"/></td>
		<td><xsl:value-of select="sluzbena_str"/></td>
		</tr> 
		</xsl:for-each> 
		</table> 
		
		<p>U nekim dvorcima moguće je i noćenje. Oni hrabri mogu provesti noć 
		u navedenim dvorcima: </p>
		<xsl:for-each select="/podaci/dvorac"> 
			<xsl:if test="@otvorenost='noćenje'">
			<ul> <li> <xsl:value-of select="naziv"/> 
			<pre>  Cijena dvosobna soba: <xsl:value-of select="cijena/cijena_odrasli"/><xsl:value-of select="cijena/@valuta"/>       Cijena obitelj: <xsl:value-of select="cijena/cijena_obitelj"/><xsl:value-of select="cijena/@valuta"/> 
			     </pre></li>
			</ul>
			
			</xsl:if>
		</xsl:for-each>
		
		<p> <br/> <br/> Najviše je dvoraca smješteno u Ujedinjenom Kraljevstvu, to su dvorci: </p>
		<xsl:for-each select="/podaci/dvorac/adresa"> 
			<xsl:if test="drzava='Ujedinjeno Kraljevstvo'">
				<ul> <li> <xsl:value-of select="../naziv"/>  </li> </ul>
			</xsl:if>
		</xsl:for-each>
		
		
		
    </article>	
	
	</div>

	<footer id="data_foot">
    Iva Lovrec <br/> FER <br/> 3. godina <br/> Smjer: Programsko inženjerstvo
    </footer>
	
	
	
	
	</body>
        </html>
    </xsl:template>
    <xsl:output method="xml" indent="yes" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" />
</xsl:stylesheet>